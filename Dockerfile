FROM openjdk:11.0.2-jdk-slim-stretch
ENV DEBIAN_FRONTEND noninteractive

ARG LIBREOFFICE_VERSION=6.4.4

COPY ./requirements.txt /code/requirements.txt
COPY src /code
COPY entrypoint.sh /docker/bin/entrypoint.sh

RUN echo 'deb http://deb.debian.org/debian stretch contrib' > /etc/apt/sources.list.d/stretch-contrib.list \
    && apt update \
    && apt install -y --no-install-recommends \
        wget \
        ttf-mscorefonts-installer \
        ttf-liberation \
        ttf-wqy-zenhei \
        fonts-arphic-ukai \
        fonts-arphic-uming \
        python3 \
        libglib2.0-0 \
        libcairo2 \
        libsm6 \
        libice6 \
    && wget https://bootstrap.pypa.io/get-pip.py \
    && python3 get-pip.py \
    && rm get-pip.py \
    && wget https://download.documentfoundation.org/libreoffice/stable/${LIBREOFFICE_VERSION}/deb/x86_64/LibreOffice_${LIBREOFFICE_VERSION}_Linux_x86-64_deb.tar.gz \
    && tar zxf LibreOffice_${LIBREOFFICE_VERSION}_Linux_x86-64_deb.tar.gz \
    && rm -rf LibreOffice_*/DEBS/libobasis5.2-gnome-integration* \
    && rm -rf LibreOffice_*/DEBS/libreoffice5.2-debian-menus* \
    && rm -rf LibreOffice_*/DEBS/libobasis5.2-kde-integration* \
    && dpkg -i LibreOffice_*/DEBS/*.deb \
    && rm -rf LibreOffice_* LibreOffice_${LIBREOFFICE_VERSION}_Linux_x86-64_deb.tar.gz \
    && pip install -r /code/requirements.txt \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* /var/cache/apt/archives/*.deb /var/cache/apt/*cache.bin /root/.cache/pip \
    && chmod a+x /docker/bin/entrypoint.sh

WORKDIR /code
ENV UNO_PATH /opt/libreoffice6.4
ENV FLASK_APP app.py
ENV FLASK_HOST 0.0.0.0
ENV FLASK_PORT 5000

ENTRYPOINT ["/docker/bin/entrypoint.sh"]