#!/usr/bin/env bash

set -e

PYTHONPATH=/opt/libreoffice6.4/program/python.bin /usr/local/bin/unoconv --listener --server=0.0.0.0 --port=2002 & flask run --host=$FLASK_HOST --port=$FLASK_PORT