from flask import Flask, request, abort, make_response
from flask_restplus import Api, Resource, fields, reqparse
from werkzeug.datastructures import FileStorage
import subprocess
import os
import sys
import json
import tempfile
import mimetypes
from uptime import uptime

BASE_PATH = os.path.dirname(os.path.realpath(__file__))
UNOCONV_BIN = '/usr/local/bin/unoconv'
LIBRE_PYTHON_BIN = '/opt/libreoffice6.4/program/python.bin'
j = json.loads(open(os.path.join(BASE_PATH, 'formats.json')).read())

app = Flask(__name__)
api = Api(app, version='1.0', title='Unoconv API')

upload_parser = api.parser()
upload_parser.add_argument('file', location='files', type=FileStorage, required=True)

@api.route("/unoconv/<format>")
@api.doc(params={'format': 'Format result by conversion', 'file': 'File to convert'})
@api.expect(upload_parser, validate=True)
@api.doc('Convert an uploaded file to format specified')
class UnoconvHandle(Resource):
    def post(self, format):
        file = request.files['file']
        fileinfo = os.path.splitext(file.filename)
        tmp = tempfile.NamedTemporaryFile(delete=False, suffix=fileinfo[1])
        file.save(tmp.name)

        env = dict(os.environ, PYTHONPATH=LIBRE_PYTHON_BIN)
        cmd = [UNOCONV_BIN, '--stdout', '-p', '2002', '-f', format, tmp.name]
        out = subprocess.check_output(cmd, env=env)
        mimetypes.init()
        response = make_response(out, 200)
        response.headers.set('Content-Type', mimetypes.types_map['.%s' % format])
        response.headers.set('Content-Disposition', 'attachment', filename='%s.%s' % (fileinfo[0], format))

        return response

@api.route("/unoconv/formats")
class UnoconvFormats(Resource):
    @api.doc('List of available formats', responses={404: 'Not Found', 200: 'Success'})
    def get(self):
        return j

@api.route("/unoconv/formats/<group>")
class UnoconvFormatsGroup(UnoconvFormats):
    @api.doc('List of available extensions for group', responses={404: 'Not Found', 200: 'Success'})
    @api.doc(params={'group': 'One of: %s' % ', '.join(j.keys())})
    def get(self, group):
        j = super().get()

        if group is not None:
            if group not in j:
                api.abort(404, 'Format group "%s" not available' % group)
            return j[group]
        return j

@api.route("/healthz")
class Uptime(UnoconvFormats):
    @api.doc('Get uptime', responses={200: 'Success'})
    def get(self):
        return {'uptime': uptime()}
